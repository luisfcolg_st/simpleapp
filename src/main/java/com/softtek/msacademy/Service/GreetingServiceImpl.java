package com.softtek.msacademy.Service;

import com.softtek.msacademy.model.Greet;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class GreetingServiceImpl implements  GreetingService {


    public GreetingServiceImpl() {

    }

    public Greet getGreet(int count) {
        Greet greet  = new Greet();
        greet.setDateTime(LocalDateTime.now());
        greet.setGreeting("Greetings from application");
        greet.setCount(count);
        return greet;
    }
}
