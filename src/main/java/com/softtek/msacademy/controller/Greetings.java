package com.softtek.msacademy.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.softtek.msacademy.Service.GreetingServiceImpl;
import com.softtek.msacademy.model.Greet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class Greetings {

    @Autowired
    GreetingServiceImpl greetingsService;

    public int count = 0;
    private final ObjectMapper objectMapper;

    public Greetings(ObjectMapper objectMapper){
        this.objectMapper = objectMapper;
    }

    @GetMapping(
            value = "/luis/greetings",
            produces = {APPLICATION_JSON_VALUE}
    )
    public ResponseEntity Greetings() {
        count++;
        String objectToJSON = "";
        Greet greet = greetingsService.getGreet(count);
        try {
            objectToJSON = objectMapper.writeValueAsString(greet);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return new ResponseEntity(objectToJSON,HttpStatus.OK);
    }
}
