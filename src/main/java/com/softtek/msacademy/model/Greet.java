package com.softtek.msacademy.model;

import java.time.LocalDateTime;
import java.time.LocalTime;

public class Greet {
    private int count;
    private String greeting;
    private LocalDateTime dateTime;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }


}
